
<html lang="en">
<?php 
$page_name = 'Users';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> New Specialization</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/add_specialization.php">Add Specialization</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <form action="view_specializations.php" method="post">
                <div class="form-group p-3">
                    <label for=""> Specialization name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Specialization name">
                    <button type="submit" class="btn btn-primary pull-right m-1" name="add_specialization"> Save</button>
                </div>
            </form>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>