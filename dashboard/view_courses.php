
<html lang="en">
<?php 
$page_name = 'Courses';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
   <?php include('get_courses.php'); 
$file_types=['PDF' , 'Word' , 'Video' , 'Image' , 'Power point'];
$status=['Pending' , 'Published'];

   ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Courses</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/view_courses.php">Courses</a></li>
        </ul>
      </div>
      <a href="add_course.php" class="btn btn-primary mb-5"> Add Course</a>

      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <?php include('errors.php');  ?>

          <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>Subject name</th>
                    <th>Uploaded date</th>
                    <th>Status</th>
                    <th>Type</th>
                    <th>Image</th>
                    <th>Url</th>
                    <th>Uploaded by</th>
                    <th>Course category</th>
                    <th>Course grade</th>
                    <th>source file </th>
                    <th>Edit</th>
                    <th>Delete</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(mysqli_num_rows($courses) == 0){
                  ?>
                  <tr>
                    <td colspan=11>
                      No courses are added
                    </td>
                  </tr>
                <?php }else{
                  foreach($courses as $course):?>
                  <tr class="text-center">
                      <td> <?= $course['name']?></td>
                      <td> <?= $course['uploaded_date']?></td>
                      <td> <?= $status[$course['status']]?></td>
                      <td> <?= $file_types[$course['type']]?></td>
                      <td>
                      <?php if(!empty($course['image'])):?>
                        <img src="/dashboard/images/course_images/<?= $course['image']?>"  width="30" height="30" alt="">
                        <?php else:?>
                          <img src="/dashboard/images/course_images/download.jpeg"  width="30" height="30" alt="">
                        <?php endif;?>
                      </td>
                      <td> 
                        <?php if(!empty($course['file_url'])):?>
                        <a href="<?= $course['file_url']?>"><i class="fa fa-link"></i></a>  
                      </td>
                      <?php else: echo '-'; endif;?>
                      <td> <?= $course['uploader_name']?></td>
                      <td> <?= $course['category_name']?></td>
                      <td> <?= $course['grade_name']?></td>
                      <td>
                      <?php 
                        if(!empty($course['source_file']) && $course['type']== 0):?>
                        <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-pdf-o"></i></a>
                        <?php elseif(!empty($course['source_file']) && $course['type']== 1): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-word-o"></i></a>
                          <?php elseif(!empty($course['source_file']) && $course['type']== 3): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-image-o"></i></a>
                          <?php elseif(!empty($course['source_file']) && $course['type']== 4): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-powerpoint-o"></i></a>
                          <?php else: echo '-'; endif;?>
                      </td>
                      <td> 
                        <a  class="btn btn-primary admin_button " href="edit_course.php?course_id=<?= $course['id'];?>">
                          <i class="fa fa-edit"></i>
                        </a> 
                      </td>
                      <td>
                      <form action="view_courses.php" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="course_id" value="<?=$course['id'];?>">
                          <button type="submit" name="delete_course" class="btn btn-primary  "><i class="fa fa-trash"></i></button>
                      </form>    
                      </td>
                    </tr>
                  <?php endforeach;
                  }?>
                </tbody>
              </table>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>