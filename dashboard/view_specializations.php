
<html lang="en">
<?php 
$page_name = 'courses';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
   <?php include('get_specializations.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Specializations</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/view_specializations.php">Specializations</a></li>
        </ul>
      </div>
      <?php include('../errors.php');?>
      <a href="add_specialization.php" class="btn btn-primary mb-5"> Add Specializations</a>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Specializations name</th>
                    <th>Specializations Edit</th>
                    <th>Specializations Delete</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(mysqli_num_rows($specializations) == 0){
                  ?>
                  <tr>
                    <td colspan=4>
                      No Specializations are added
                    </td>
                  </tr>
                <?php }else{
                  while($specialization = $specializations->fetch_assoc()) :?>
                  <tr>
                      <td> <?= $specialization['id']?></td>
                      <td> <?= $specialization['name']?></td>
                      <td> 
                        <a  class="btn btn-primary admin_button " href="edit_specialization.php?specialization_id=<?= $specialization['id'];?>">
                          <i class="fa fa-edit"></i>
                        </a> 
                      </td>
                      <td>
                      <form action="view_specializations.php" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="specialization_id" value="<?=$specialization['id'];?>">
                          <button type="submit" name="delete_specialization" class="btn btn-primary  "><i class="fa fa-trash"></i></button>
                      </form>    
                      </td>
                    </tr>
                  <?php endwhile;
                  }?>
                </tbody>
              </table>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>