<?php

if(!empty($errors)): ?>

<div>
    <?php foreach($errors as $error): ?>
        <div class="alert alert-danger" role="alert">
        <?= $error ?>
        </div>
    <?php endforeach;?>
</div>
<?php elseif(!empty($success)) : ?>
    <div>
        <div class="alert alert-success" role="alert">
        <?= $success ?>
        </div>
</div>
<?php return; endif;?>
