
<html lang="en">
<?php 
$page_name = 'Users';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('get_grade_by_id.php'); ?>
   <?php include('sidebar.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Edit Grade</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/edit_grade.php">Edit Grade</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <form action="view_course_grade.php" method="post">
                <div class="form-group p-3">
                    <label for=""> Grade name</label>
                    <input type="text" class="form-control" name="name"  value="<?= $grade['name'];?>" placeholder="Enter Grade name">
                    <input type="hidden" name="grade_id" value="<?=$grade['id'];?>">
                    <button type="submit" class="btn btn-primary pull-right m-1" name="update_grade"> Save</button>
                </div>
            </form>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>