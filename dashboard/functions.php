<?php

include('connection/connection.php');
$errors = array();
$success = '';


if (session_status() == PHP_SESSION_NONE) {
    session_start();
}    
//add course category
if(isset($_POST['add_cat'])){
   
    $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
    //form validation
    if(empty($name)){ array_push($errors , " Category name is required");}
  
    //check db for checking existing category name
    $cat_check_query = "SELECT * FROM file_category WHERE name = '$name'  LIMIT 1";
    $result = mysqli_query($db , $cat_check_query);
    $cat = mysqli_fetch_assoc($result);

    if($cat){  array_push($errors , ' name already exists'); }

    // register the user if no errors exist

    if(count($errors) == 0){
        $query = "INSERT INTO file_category (name) VALUES ('$name')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }  
        $success = ' Category added';
    }
}
//delete category
if(isset($_POST['delete_category'])){
    $cat_id = $_POST['cat_id'];
    $delete_category_query = "DELETE FROM file_category WHERE id='$cat_id'";
    if ($db->query($delete_category_query) === TRUE) {
        $success = " category deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}

// update category
if(isset($_POST['update_cat'])){
    $cat_id = (!empty($_POST['cat_id']))? mysqli_real_escape_string($db , $_POST['cat_id']) :'';

    $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
   
    //check db for checking existing username
    $cat_check_query = "SELECT * FROM file_category WHERE (name = '$name' ) AND id != '$cat_id' LIMIT 1";
    $results = mysqli_query($db , $cat_check_query);
    $cat_check_exs = mysqli_fetch_assoc($results);
    if($cat_check_exs){
         array_push($errors , ' name already exists');
    }
 
    // register the user if no errors exist
    if(count($errors) == 0){        
            $query = "UPDATE file_category SET  name = '$name' Where id = $cat_id";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        $cat = $results ->fetch_array(MYSQLI_ASSOC);
        $success = "category updated successfully";

        }
}
// add course grade
if(isset($_POST['add_grade'])){
   
    $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
    //form validation
    if(empty($name)){ array_push($errors , " Grade name is required");}
  
    //check db for checking existing category name
    $grade_check_query = "SELECT * FROM file_grade WHERE name = '$name'  LIMIT 1";
    $result = mysqli_query($db , $grade_check_query);
    $grade = mysqli_fetch_assoc($result);

    if($grade){  array_push($errors , ' name already exists'); }

    // register the user if no errors exist

    if(count($errors) == 0){
        $query = "INSERT INTO file_grade (name) VALUES ('$name')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }  
        $success = ' Grade added';
    }
}
//upgrade course grade
if(isset($_POST['update_grade'])){
    $grade_id = (!empty($_POST['grade_id']))? mysqli_real_escape_string($db , $_POST['grade_id']) :'';

    $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
   
    //check db for checking existing username
    $grade_check_query = "SELECT * FROM file_grade WHERE (name = '$name' ) AND id != '$grade_id' LIMIT 1";
    $results = mysqli_query($db , $grade_check_query);
    $grade_check_exs = mysqli_fetch_assoc($results);
    if($grade_check_exs){
         array_push($errors , ' name already exists');
    }
 
    // register the user if no errors exist
    if(count($errors) == 0){        
            $query = "UPDATE file_grade SET  name = '$name' Where id = $grade_id";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        $grade = $results ->fetch_array(MYSQLI_ASSOC);
        $success = "Grade updated successfully";

        }
}
//delete grade
if(isset($_POST['delete_grade'])){
    $grade_id = $_POST['grade_id'];
    $delete_grade_query = "DELETE FROM file_grade WHERE id='$grade_id'";
    if ($db->query($delete_grade_query) === TRUE) {
        $success = " grade deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}
//add course category
if(isset($_POST['add_course'])){

    $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
    $description =(!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']): '';
    $uploaded_date = date("Y/m/d");
    $status =(!empty($_POST['status']))? mysqli_real_escape_string($db , $_POST['status']): 0;
    $type =(!empty($_POST['type']))? mysqli_real_escape_string($db , $_POST['type']): 0;
    $file_url =(!empty($_POST['file_url']))? mysqli_real_escape_string($db , $_POST['file_url']): '';
    $uploaded_by =(!empty($_POST['uploaded_by']))? (int) $_POST['uploaded_by']: '';
    $file_cat =(!empty($_POST['file_cat']))? (int) $_POST['file_cat']: '';
    $file_grade =(!empty($_POST['file_grade']))? (int) $_POST['file_grade']: '';
    $image = '';
    $source_file = '';

    //form validation
    if(empty($name)){ array_push($errors , " Course name is required");}
    if(empty($description)){ array_push($errors , " description  is required");}
    if(empty($uploaded_date)){ array_push($errors , " uploaded date  is required");}
    if(empty($uploaded_by)){ array_push($errors , " cant get uploading user");}
    if($type == 0 || $type == 1){
        if(empty($_FILES["source_file"]['name'])){ array_push($errors , " File source cant be empty when the type is file not video");}
    }else{
        if(empty($file_url)){ array_push($errors , " File url cant be empty when the type is video");}

    }
    
    //check db for checking existing category name
    $course_check_query = "SELECT * FROM files WHERE name = '$name'  LIMIT 1";
    $result = mysqli_query($db , $course_check_query);
    $course = mysqli_fetch_assoc($result);

    if($course){  array_push($errors , ' this name already exists'); }
    if(!empty($_FILES["image"]['name'])){
        if(isset($_POST['is_frontend'])):
        $uploaddir = '/dashboard/images/course_images/';
        else:         
        $uploaddir = '/images/course_images/';
        endif;
            
        $uploadfile = $uploaddir . basename($_FILES['image']['name']);

        echo "<p>";
        $file_upload = move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT.$uploadfile);
        if ($file_upload) {
            $image = basename($_FILES['image']['name']);
        } else {
          array_push($errors , 'image upload failed'); 
        }
    }
    if(!empty($_FILES["source_file"]['name'])){
        if(isset($_POST['is_frontend'])):
        $uploaddir = '/dashboard/courses/';
        else:
        $uploaddir = '/courses/';
        endif;
        $uploadfile = $uploaddir . basename($_FILES['source_file']['name']);

        echo "<p>";
        $file_upload = move_uploaded_file($_FILES['source_file']['tmp_name'], SITE_ROOT.$uploadfile);
        if ($file_upload) {
            $source_file = basename($_FILES['source_file']['name']);
        } else {
          array_push($errors , 'Course file upload failed'); 
        }
    }
    // register the course if no errors exist
    
    if(count($errors) == 0){
        $query = "INSERT INTO files (name , description , uploaded_date , status , type , file_url , uploaded_by , file_cat , file_grade , image ,  source_file) VALUES ('$name' , '$description' , '$uploaded_date' , '$status' , '$type' , '$file_url' , '$uploaded_by' , '$file_cat' , '$file_grade' , '$image' ,  '$source_file')";

        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }  
        $success = 'Course added';
 
        if(isset($_SESSION['type'] ) && $_SESSION['type'] == 0):
        header('location: /dashboard/view_courses.php');
        else: 
            header('location:/courses.php');
        endif;
    }
}
// update course
if(isset($_POST['update_course'])){
    $course_id = (!empty($_POST['course_id']))? mysqli_real_escape_string($db , $_POST['course_id']) :'';
    $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
    $description =(!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']): '';
    $status =(!empty($_POST['status']))? mysqli_real_escape_string($db , $_POST['status']): 0;
    $type =(!empty($_POST['type']))? mysqli_real_escape_string($db , $_POST['type']): 0;
    $file_url =(!empty($_POST['file_url']))? mysqli_real_escape_string($db , $_POST['file_url']): '';
    $uploaded_by =(!empty($_POST['uploaded_by']))? (int) $_POST['uploaded_by']: '';
    $file_cat =(!empty($_POST['file_cat']))? (int) $_POST['file_cat']: '';
    $file_grade =(!empty($_POST['file_grade']))? (int) $_POST['file_grade']: '';
    $image = '';
    $source_file = '';
    if(isset($_POST['from_profile'])):
        $header='location: /my-profile.php';
        $uploaddirlink = '/dashboard/images/course_images/';
        $uploaddircourse = '/dashboard/courses/';
    else:
        $header='location: /dashboard/view_courses.php';
        $uploaddirlink = '/images/course_images/';
        $uploaddircourse = '/courses/';

    endif;

    if(empty($name)){ array_push($errors , " Course name is required");}
    if(empty($description)){ array_push($errors , " description  is required");}
    if(empty($uploaded_by)){ array_push($errors , " cant get uploading user");}
 
    //check db for checking existing username
    $course_check_query = "SELECT * FROM files WHERE (name = '$name' ) AND id != '$course_id' LIMIT 1";
   
    $result = mysqli_query($db , $course_check_query);
    $course = mysqli_fetch_assoc($result);
    if($course){
        array_push($errors , 'course name already exists'); 
    }
    if(!empty($_FILES["image"]['name'])){
 
        $uploaddir = $uploaddirlink;
        $uploadfile = $uploaddir . basename($_FILES['image']['name']);

        echo "<p>";
        $file_upload = move_uploaded_file($_FILES['image']['tmp_name'], SITE_ROOT.$uploadfile);
        if ($file_upload) {
            $image = basename($_FILES['image']['name']);
        } else {
          array_push($errors , 'image upload failed'); 
        }
    }
    if(!empty($_FILES["source_file"]['name'])){
 
        $uploaddir = $uploaddircourse;
        $uploadfile = $uploaddir . basename($_FILES['source_file']['name']);

        echo "<p>";
        $file_upload = move_uploaded_file($_FILES['source_file']['tmp_name'], SITE_ROOT.$uploadfile);
        if ($file_upload) {
            $source_file = basename($_FILES['source_file']['name']);
        } else {
          array_push($errors , 'Course file upload failed'); 
        }
    }
    // register the user if no errors exist
    if(count($errors) == 0){
        if(!empty($image)):
            if($type == 0 || $type == 1 || $type == 3 || $type == 4):
                $query = "UPDATE files SET name = '$name' , description = '$description' ,  status = '$status' , type = '$type' , file_cat='$file_cat' , file_grade = '$file_grade' , uploaded_by = '$uploaded_by' , source_file = '$source_file' , image = '$image' Where id = $course_id";
            elseif($type == 2):
                $query = "UPDATE files SET name = '$name' , description = '$description' ,  status = '$status' , type = '$type' , file_cat='$file_cat' , file_grade = '$file_grade' , uploaded_by = '$uploaded_by' , file_url = '$file_url' , image = '$image' Where id = $course_id";
            endif;
        else:
            if($type == 0 || $type == 1 || $type == 3 || $type == 4):
                if(!empty($source_file)):
                   $query = "UPDATE files SET name = '$name' , description = '$description' ,  status = '$status' , type = '$type' , file_cat='$file_cat' , file_grade = '$file_grade' , uploaded_by = '$uploaded_by' , source_file = '$source_file'  Where id = $course_id";
                else:
                    $query = "UPDATE files SET name = '$name' , description = '$description' ,  status = '$status' , type = '$type' , file_cat='$file_cat' , file_grade = '$file_grade' , uploaded_by = '$uploaded_by'  Where id = $course_id";
                endif;
             elseif($type == 2):
                $query = "UPDATE files SET name = '$name' , description = '$description' ,  status = '$status' , type = '$type' , file_cat='$file_cat' , file_grade = '$file_grade' , uploaded_by = '$uploaded_by' , file_url = '$file_url'  Where id = $course_id";
            endif;        
        endif;
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        $course = $result ->fetch_array(MYSQLI_ASSOC);
      
        $success = "course updated successfully";
        if(isset($_POST['from_profile'])):
            header('location: /my-profile.php');
        else:
            header('location: /dashboard/view_courses.php');
        endif;
    }
}
//delete grade
if(isset($_POST['delete_course'])){
    $course_id = $_POST['course_id'];
    $delete_course_query = "DELETE FROM files WHERE id='$course_id'";
    if ($db->query($delete_course_query) === TRUE) {
        $success = " Course deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}

// add specialization
if(isset($_POST['add_specialization'])){
   
    $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
    //form validation
    if(empty($name)){ array_push($errors , " Specialization name is required");}
  
    //check db for checking existing category name
    $specialization_check_query = "SELECT * FROM specializations WHERE name = '$name'  LIMIT 1";
    $result = mysqli_query($db , $specialization_check_query);
    $specialization = mysqli_fetch_assoc($result);

    if($specialization){  array_push($errors , ' name already exists'); }

    // register the user if no errors exist

    if(count($errors) == 0){
        $query = "INSERT INTO specializations (name) VALUES ('$name')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }  
        $success = ' Specialization added';
    }
}
//upgrade specialization
if(isset($_POST['update_specialization'])){
    $specialization_id = (!empty($_POST['specialization_id']))? mysqli_real_escape_string($db , $_POST['specialization_id']) :'';

    $name = (!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']) :'';
   
    //check db for checking existing username
    $specialization_check_query = "SELECT * FROM specializations WHERE (name = '$name' ) AND id != '$specialization_id' LIMIT 1";
    $results = mysqli_query($db , $specialization_check_query);
    $specialization_check_exs = mysqli_fetch_assoc($results);
    if($specialization_check_exs){
         array_push($errors , ' name already exists');
    }
 
    // register the user if no errors exist
    if(count($errors) == 0){        
            $query = "UPDATE specializations SET  name = '$name' Where id = $specialization_id";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }
        $specialization = $results ->fetch_array(MYSQLI_ASSOC);
        $success = "Specialization updated successfully";

        }
}
//delete specialization
if(isset($_POST['delete_specialization'])){
    $specialization_id = $_POST['specialization_id'];
    $delete_specialization_query = "DELETE FROM specializations WHERE id='$specialization_id'";
    if ($db->query($delete_specialization_query) === TRUE) {
        $success = " specialization deleted successfully";

      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}
//add course category
if(isset($_POST['add_contact_us'])){
   
    $name =(!empty($_POST['name']))? mysqli_real_escape_string($db , $_POST['name']): '';
    $email =(!empty($_POST['email']))? mysqli_real_escape_string($db , $_POST['email']): '';
    $subject =(!empty($_POST['subject']))? mysqli_real_escape_string($db , $_POST['subject']): '';
    $message =(!empty($_POST['message']))? mysqli_real_escape_string($db , $_POST['message']): '';

    //form validation
    if(empty($name)){ array_push($errors , " Name is required");}
    if(empty($email)){ array_push($errors , " Email is required");}
    if(empty($subject)){ array_push($errors , " Subject name is required");}
    if(empty($message)){ array_push($errors , " Message name is required");}



    // register the user if no errors exist

    if(count($errors) == 0){
        $query = "INSERT INTO contact_us (name , email , subject , message) VALUES ('$name' , '$email' , '$subject' , '$message')";
        $result_of_the_query = mysqli_query($db , $query);
        if($result_of_the_query == false){
            echo mysqli_error($db);
            die();
        }  
        echo '<div class="alert container alert-success text-center" role="alert">
            Message sent
      </div>';
        }
}
//delete specialization
if(isset($_POST['delete_contact_us'])){
    $id = $_POST['message_id'];
    $delete_contact_us_query = "DELETE FROM contact_us WHERE id='$id'";
    if ($db->query($delete_contact_us_query) === TRUE) {
        $success = " Message deleted successfully";
      } else {
        echo '<div class="alert alert-danger text-center" role="alert">
       '.$db->error.'
     </div>';
      }
}
