
<html lang="en">
<?php 
$page_name = 'Contact us';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
   <?php include('get_contact_us.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Contact us messages</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/contact_us.php">Contact Us Messages</a></li>
        </ul>
      </div>
      <?php include('../errors.php');?>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Message Delete</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(mysqli_num_rows($contactus) == 0){
                  ?>
                  <tr>
                    <td colspan=4>
                      No Messages are Sent
                    </td>
                  </tr>
                <?php }else{
                  while($message = $contactus->fetch_assoc()) :?>
                  <tr>
                      <td> <?= $message['id']?></td>
                      <td> <?= $message['name']?></td>
                      <td> <?= $message['email']?></td>
                      <td> <?= $message['subject']?></td>
                      <td> <?= $message['message']?></td>
                      <td>
                      <form action="contact_us.php" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="message_id" value="<?=$message['id'];?>">
                          <button type="submit" name="delete_contact_us" class="btn btn-primary  "><i class="fa fa-trash"></i></button>
                      </form>    
                      </td>
                    </tr>
                  <?php endwhile;
                  }?>
                </tbody>
              </table>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>