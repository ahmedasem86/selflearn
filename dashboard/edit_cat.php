
<html lang="en">
<?php 
$page_name = 'Users';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('get_cat_by_id.php'); ?>
   <?php include('sidebar.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Edit Category</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/add_cat.php">Add Category</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <form action="view_course_cat.php" method="post">
                <div class="form-group p-3">
                    <label for=""> Category name</label>
                    <input type="text" class="form-control" name="name"  value="<?= $category['name'];?>" placeholder="Enter Category name">
                    <input type="hidden" name="cat_id" value="<?=$category['id'];?>">
                    <button type="submit" class="btn btn-primary pull-right m-1" name="update_cat"> Save</button>
                </div>
            </form>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>