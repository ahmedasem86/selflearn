
<html lang="en">
<?php 
$page_name = 'courses';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
   <?php include('get_course_grade.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Courses grades</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/view_course_grade.php">Courses grades</a></li>
        </ul>
      </div>
      <?php include('../errors.php');?>
      <a href="add_grade.php" class="btn btn-primary mb-5"> Add Grade</a>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Grade name</th>
                    <th>Grade Edit</th>
                    <th>Grade Delete</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(mysqli_num_rows($courses_grades) == 0){
                  ?>
                  <tr>
                    <td colspan=4>
                      No grades are added
                    </td>
                  </tr>
                <?php }else{
                  while($grade = $courses_grades->fetch_assoc()) :?>
                  <tr>
                      <td> <?= $grade['id']?></td>
                      <td> <?= $grade['name']?></td>
                      <td> 
                        <a  class="btn btn-primary admin_button " href="edit_grade.php?grade_id=<?= $grade['id'];?>">
                          <i class="fa fa-edit"></i>
                        </a> 
                      </td>
                      <td>
                      <form action="view_course_grade.php" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="grade_id" value="<?=$grade['id'];?>">
                          <button type="submit" name="delete_grade" class="btn btn-primary  "><i class="fa fa-trash"></i></button>
                      </form>    
                      </td>
                    </tr>
                  <?php endwhile;
                  }?>
                </tbody>
              </table>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>