
<html lang="en">
<?php 
$page_name = 'Courses';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
   <?php include('get_course_cat.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Courses categories</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/view_course_cat.php">Courses Categories</a></li>
        </ul>
      </div>
      <?php include('../errors.php');?>
      <a href="add_cat.php" class="btn btn-primary mb-5"> Add Category</a>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Category name</th>
                    <th>Category Edit</th>
                    <th>Category Delete</th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(mysqli_num_rows($courses_categories) == 0){
                  ?>
                  <tr>
                    <td colspan=4>
                      No categories are added
                    </td>
                  </tr>
                <?php }else{
                  while($category = $courses_categories->fetch_assoc()) :?>
                  <tr>
                      <td> <?= $category['id']?></td>
                      <td> <?= $category['name']?></td>
                      <td> 
                        <a  class="btn btn-primary admin_button " href="edit_cat.php?cat_id=<?= $category['id'];?>">
                          <i class="fa fa-edit"></i>
                        </a> 
                      </td>
                      <td>
                      <form action="view_course_cat.php" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="cat_id" value="<?=$category['id'];?>">
                          <button type="submit" name="delete_category" class="btn btn-primary  "><i class="fa fa-trash"></i></button>
                      </form>    
                      </td>
                    </tr>
                  <?php endwhile;
                  }?>
                </tbody>
              </table>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>