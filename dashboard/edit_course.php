
<html lang="en">
<?php 
$page_name = 'Users';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php 
    include('header.php'); 
    include('get_course_by_id.php'); 
    include('sidebar.php'); 
    include('get_course_cat.php');
    include('get_course_grade.php');

    ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Edit Course</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/edit_course.php?<?= $course['id'];?>">Edit Course</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
        <?php include('../errors.php'); ?>

          <div class="tile">
          <form action="" method="post" class="p-5 row"enctype="multipart/form-data">

              <div class="form-group col-md-6 p-3">
                  <label for=""> Course name</label>
                  <input type="text" class="form-control" name="name" value="<?=$course['name']?>" placeholder="Enter Course name">
              </div>
              <div class="form-group col-md-6 p-3">
                  <label for=""> Course description</label> <br>
                  <textarea name="description" lass="form-control" cols="67" rows="5"><?=$course['description']?></textarea>
              </div>
              <div class="form-group col-md-6 p-3">
                  <label for=""> Status</label>
                  <select class="form-select form-control" name="status" aria-label="Default select example">
                      <option value="0" <?=($course['status'] == 0)?'selected':'';?>>pending</option>
                      <option value="1" <?=($course['status'] == 1)?'selected':'';?>>approved</option>
                  </select>                
                </div>
                <div class="form-group col-md-6 p-3">
                  <label for=""> Type</label>
                  <select class="form-select form-control" name="type" aria-label="Default select example">
                      <option value="0" <?=($course['type'] == 0)?'selected':'';?>>Pdf</option>
                      <option value="1" <?=($course['type'] == 1)?'selected':'';?>>Word</option>
                      <option value="2" <?=($course['type'] == 2)?'selected':'';?>>Video from url</option>
                      <option value="3" <?=($course['type'] == 3)?'selected':'';?>>Image</option>
                      <option value="4" <?=($course['type'] == 4)?'selected':'';?>>Power point</option>
                  </select>                
                </div>
                <div class="form-group col-md-6 p-3">
                  <label for=""> Course url</label>
                  <input type="text" class="form-control" name="file_url" value="<?= $course['file_url'] ?>" placeholder="Enter Course name">
              </div>
              <div class="form-group  col-md-6 p-3">
                  <label for=""> Course category</label>
                  <select class="form-select form-control" name="file_cat" aria-label="Default select example">
                      <?php foreach($courses_categories as $cat): ?> 
                    <option value="<?=$cat['id']?>" <?=($course['file_cat'] == $cat['id'])?'selected':'';?>><?=$cat['name']?></option>
                        <?php endforeach; ?>
                  </select>                
                </div>
                <div class="form-group  col-md-6 p-3">
                  <label for=""> Course grade</label>
                  <select class="form-select form-control" name="file_grade" aria-label="Default select example">
                  <?php foreach($courses_grades as $grade): ?> 
                    <option value="<?=$grade['id']?>" <?=($course['file_grade'] == $grade['id'])?'selected':'';?>><?=$grade['name']?></option>
                        <?php endforeach; ?>
                  </select>                
                </div>
                <div class="form-group col-md-6 p-3">
                  <label for=""> Cover photo :</label><br>
                  <img src="/dashboard/images/course_images/<?= $course['image'] ?>" width="50" height="50" class="mb-1" alt="">
                  <input type="file" class="form-control" name="image">
              </div>
         
              <input type="hidden" name="uploaded_by" value="<?= $_SESSION['id']?>">
              <div class="form-group  col-md-6 p-3">
                  <label for=""> Source file : </label> 
                  <?php if(!empty($course['source_file']) && $course['type']== 0):?>
                        <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-pdf-o"></i> <?= $course['source_file']?></a>
                        <?php elseif(!empty($course['source_file']) && $course['type']== 1): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-word-o"></i> <?= $course['source_file']?></a>
                          <?php else: echo '-'; endif;?>
                  <input type="file" class="form-control source_file" name="source_file">
              </div>
              
                <input type="hidden" name="course_id" value="<?=$course['id']?>">
              <button type="submit" class="btn btn-primary col-md-12 pull-right m-1" name="update_course"> Save</button>

          </form>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>