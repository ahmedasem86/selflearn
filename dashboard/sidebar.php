  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <ul class="app-menu">
        <li><a class="app-menu__item" href="/index.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Go to front end</span></a></li>
        <li><a class="app-menu__item <?= ($_SERVER['REQUEST_URI'] == "/dashboard/view_users.php")? 'active' : '';?>" href="/dashboard/view_users.php"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Users</span></a></li>
        <li><a class="app-menu__item <?= ($_SERVER['REQUEST_URI'] == "/dashboard/view_courses.php")? 'active' : '';?>" href="/dashboard/view_courses.php"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Courses</span></a></li>
        <li><a class="app-menu__item <?= ($_SERVER['REQUEST_URI'] == "/dashboard/view_course_cat.php")? 'active' : '';?>" href="/dashboard/view_course_cat.php"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Courses categories</span></a></li>
        <li><a class="app-menu__item <?= ($_SERVER['REQUEST_URI'] == "/dashboard/view_course_grade.php")? 'active' : '';?>" href="/dashboard/view_course_grade.php"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Courses grades</span></a></li>
        <li><a class="app-menu__item <?= ($_SERVER['REQUEST_URI'] == "/dashboard/view_specializations.php")? 'active' : '';?>" href="/dashboard/view_specializations.php"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Specializations</span></a></li>
        <li><a class="app-menu__item <?= ($_SERVER['REQUEST_URI'] == "/dashboard/contact_us.php")? 'active' : '';?>" href="/dashboard/contact_us.php"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Contact Us</span></a></li>

      </ul>
    </aside>