
<html lang="en">
<?php 
$page_name = 'Users';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> New Grade</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/add_grade.php">Add Grade</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <form action="view_course_grade.php" method="post">
                <div class="form-group p-3">
                    <label for=""> Grade name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter Grade name">
                    <button type="submit" class="btn btn-primary pull-right m-1" name="add_grade"> Save</button>
                </div>
            </form>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>