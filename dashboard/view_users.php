
<html lang="en">
<?php 
$page_name = 'Users';
include('head.php')?>
  <body class="app sidebar-mini rtl">
   <?php include('header.php'); ?>
   <?php include('sidebar.php'); ?>
   <?php include('get_users.php'); ?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1></i> Users</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="/dashboard/users.php">users</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>id</th>
                    <th>Full name</th>
                    <th>User name</th>
                    <th>Email</th>
                    <th>phone</th>
                    <th>Address</th>
                    <th>User Type</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(mysqli_num_rows($users) == 0){
                  ?>
                  <tr>
                    <td colspan=7>
                      No users are added
                    </td>
                  </tr>
                <?php }else{
                  $user_type = [
                    'Administrator',
                    'Student',
                    'Teacher'
                  ];
                  while($user = $users->fetch_assoc()) :?>
                  <tr>
                      <td> <?= $user['id']?></td>
                      <td> <?= $user['full_name']?></td>
                      <td> <?= $user['user_name']?></td>
                      <td> <?= $user['email']?></td>
                      <td> <?= $user['phone']?></td>
                      <td> <?= $user['address']?></td>
                      <td> <?= $user_type[$user['type']];?></td>
                    </tr>
                  <?php endwhile;
                  }?>
                </tbody>
              </table>
        </div>
        </div>
      </div>
    </main>
   <?php include('footer.php')?>
  </body>
</html>