<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>
<?php include('dashboard/get_all_teachers.php'); ?>
<?php include('dashboard/get_specializations.php'); ?>


  <body>

  
    
    <div class="probootstrap-page-wrapper">
    <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1>Our Teachers</h1>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-flex-block">
                <div class="probootstrap-text probootstrap-animate" style="width:100%;">
                  <h3>We Hired Certified Teachers For Our Students</h3>
                  <p>Online Self-Learning is a website to enable teachers to give their experience and knowledge to their students and to share links, documents, YouTube videos, and exams in many subjects.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>


      
      
      <section class="probootstrap-section" style="padding-top:0px;">
        <div class="container">
       

          <div class="row">
             <form class=" col-md-12 form-inline" style="margin-bottom: 20px;">
                  <div class="col-md-3">
                    <select class=" form-control" name="specialization" id="">
                      <option value="all">Select Teacher specialization</option>
                      <?php foreach($specializations as $specialization): ?>
                      <option value="<?= $specialization['id'];?>" <?= (isset($_GET['specialization']) && $_GET['specialization'] == $specialization['id'])? 'selected' : ''; ?>><?= $specialization['name'];?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <input class="form-control col-md-12" name="keyword" value="<?= (isset($_GET['keyword']))? $_GET['keyword'] : ''?>" type="search" placeholder="Keyword" aria-label="Search">
                  </div>
                  <button class="btn btn-primary my-2 col-md-3 my-sm-0" type="submit">Search</button>
        </form>
          <?php foreach($teachers as $teacher): ?>
            <a href="my-profile.php?user_id=<?= $teacher['id']?>">
            <div class="col-md-3 col-sm-6">
              <div class="probootstrap-teacher text-center probootstrap-animate">
                <figure class="media">
                  <img style="height: 90px;"src="<?= (isset($teacher['profile_photo']) && !empty($teacher['profile_photo']))?  '/dashboard/images/profile_images/'.$teacher['profile_photo'] :'img/person_1.jpg';?>" alt="My images" class="img-responsive">
                </figure>
                <div class="text">
                  <h3><?= $teacher['full_name'];?></h3>
                  <p><?= (isset($teacher['name'])?$teacher['name']:'')?> Teacher</p>

                </div>
              </div>
            </div>
            </a>
            <?php endforeach;?>
          </div>
        </div>
      </section>
      <?php include('footer.php');?>

    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>