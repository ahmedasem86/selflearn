<!DOCTYPE html>
<html lang="en">
<?php 
include('head.php');
include('dashboard/get_course_cat.php');
include('dashboard/get_course_grade.php');
?>
  <body>

  
    <div class="probootstrap-page-wrapper">
      <!-- Fixed navbar -->
      <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1>Add new Courses</h1>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section" style="padding:1em;">
        <div class="container">
          <div class="row">
            <div class="col-md-12"> 
            <form action="add_course.php" method="post" enctype="multipart/form-data">

              <div class="form-group p-3">
                  <label for=""> Course name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter Course name">
              </div>
              <div class="form-group p-3">
                  <label for=""> Course description</label> <br>
                  <textarea name="description" lass="form-control" cols="67" rows="5"></textarea>
              </div>
                <div class="form-group p-3">
                  <label for=""> Type</label>
                  <select class="form-select form-control" name="type" aria-label="Default select example">
                        <option value="0">Pdf</option>
                        <option value="1">Word</option>
                        <option value="2">Video from url</option>
                        <option value="3">Image</option>
                        <option value="4">Power point</option>
                  </select>                
                </div>
                <div class="form-group p-3">
                  <label for=""> Course category</label>
                  <select class="form-select form-control" name="file_cat" aria-label="Default select example">
                      <?php foreach($courses_categories as $cat): ?> 
                    <option value="<?=$cat['id']?>"><?=$cat['name']?></option>
                        <?php endforeach; ?>
                  </select>                
                </div>
                <div class="form-group p-3">
                  <label for=""> Course grade</label>
                  <select class="form-select form-control" name="file_grade" aria-label="Default select example">
                  <?php foreach($courses_grades as $grade): ?> 
                    <option value="<?=$grade['id']?>"><?=$grade['name']?></option>
                        <?php endforeach; ?>
                  </select>                
                </div>
                <div class="form-group p-3">
                  <label for=""> Cover photo :</label>
                  <input type="file" class="form-control" name="image">
              </div>
              <div class="form-group p-3">
                  <label for=""> Course url</label>
                  <input type="text" class="form-control" name="file_url" placeholder="Enter Course name">
              </div>
              <input type="hidden" name="uploaded_by" value="<?= $_SESSION['id']?>">
              <div class="form-group p-3">
                  <label for=""> Source file:</label>
                  <input type="file" class="form-control" name="source_file">
              </div>
                    <input type="hidden" name="is_frontend" value="1">
              <button type="submit" class="btn btn-primary pull-right m-1" name="add_course"> Save</button>
              </form>
              
              </div>  
          </div>
        </div>
      </section>
  <?php include('footer.php');?>
    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>