<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); 
include('dashboard/get_user_by_id.php');
include('dashboard/get_user_courses.php');
include('dashboard/get_specializations.php');

$file_types=['PDF' , 'Word' , 'Video' , 'Image' , 'Power point'];
   $status=['Pending' , 'Published'];
?>
  <body>
    <div class="probootstrap-page-wrapper">
    <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1><?= $user['user_name'];?></h1>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section probootstrap-section-sm">
        <div class="">
          <div class="row">
            <div class="col-md-12">
              <div class="row probootstrap-gutter0">
                <div class="container">
                <div class="col-md-4" id="probootstrap-sidebar">
                  <div class="probootstrap-overlap probootstrap-animate">
                    <div class="probootstrap-side-menu">
                    <img width="300" height="300"src="<?= (isset($user['profile_photo']) && !empty($user['profile_photo']))?  '/dashboard/images/profile_images/'.$user['profile_photo'] :'img/person_1.jpg';?>" alt=""> 
                  </div>
                  </div>
                </div>
                <div class="col-md-7 col-md-push-1  probootstrap-animate" id="probootstrap-content">
                  <h2>Personal Info</h2>
                  <?php if(isset($_SESSION['id']) && $_SESSION['id'] == $user['id']): ?>
                  <div class="col-md-12" >
                     <a href="#" class="btn pull-right" style="color: #fff; background: #49D292; border-radius: 0;font-size: 14px;display: inline-block;padding: 8px 15px;padding-top:10px; " data-toggle="modal" data-target="#basicModal"> <i class="icon icon-edit"></i> Edit profile</a>               
                  </div>
                  <?php endif; ?>
                  <ul>
                    <li><b>Full name : </b> <?= $user['full_name'];?></li>
                    <li><b>User name : </b> <?= $user['user_name'];?></li>
                    <li><b>Email : </b> <?= $user['email'];?></li>
                    <li><b>Address : </b> <?= $user['address'];?></li>
                    <li><b>Phone : </b> <?= $user['phone'];?></li>
                    <?php if($user['type'] == 1): ?>
                    <li><b>Specialization : </b> <?= $user['name'];?></li>
                    <li><b>Description : </b> <?= $user['description'];?></li>
                      <?php endif;?>
                  </ul>
                </div>
                </div>
             
                <div class="courses container-fluid">

                <h3>My Courses uploaded</h3>
                    <table class="table ">
                      <thead class="thead-dark">
                        <tr>
                          <td>Name</td>
                          <td>Description</td>
                          <td>Uploaded date</td>
                          <td>Status</td>
                          <td>Type</td>
                          <td>Video url</td>
                          <td>Category</td>
                          <td>Grade</td>
                          <td>File</td>
                          <?php if(isset($_SESSION['id']) && $_SESSION['id'] == $user['id']): ?>
                          <td>Edit</td>
                          <td>Delete</td>
                            <?php endif;?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($my_courses as $course): ?>
                          <tr>
                        <td><?= $course['name']?></td>
                        <td><?= substr($course['description'], 0, strrpos(substr($course['description'], 0, 80), ' '))?></td>
                        <td><?= $course['uploaded_date']?></td>
                        <td><?= $status[$course['status']]?></td>
                        <td><?= $file_types[$course['type']]?></td>
                        <td><?= $course['file_url']?></td>
                        <td><?= $course['category_name']?></td>
                        <td><?= $course['grade_name']?></td>
                        <td>
                      <?php 
                        if(!empty($course['source_file']) && $course['type']== 0):?>
                        <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-pdf-o"></i></a>
                        <?php elseif(!empty($course['source_file']) && $course['type']== 1): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-word-o"></i></a>
                          <?php elseif(!empty($course['source_file']) && $course['type']== 3): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-image-o"></i></a>
                          <?php elseif(!empty($course['source_file']) && $course['type']== 4): ?>
                          <a href="/dashboard/courses/<?= $course['source_file']?>"><i class="fa fa-file-powerpoint-o"></i></a>
                          <?php else: echo '-'; endif;?>
                      </td>
                          <?php if(isset($_SESSION['id']) && $_SESSION['id'] == $user['id']): ?>
                      <td> 
                        <a  class="btn btn-primary admin_button " href="edit_course.php?course_id=<?= $course['id'];?>">
                          <i class="fa fa-edit"></i>
                        </a> 
                      </td>
                      <td>
                      <form action="my-profile.php" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="course_id" value="<?=$course['id'];?>">
                          <button type="submit" name="delete_course" class="btn btn-primary  "><i class="fa fa-trash"></i></button>
                      </form>    
                      </td>
                      <?php endif; ?>

                        </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('footer.php');?>


    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
  <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Edit profile</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                </div>
                <div class="modal-body">    
                    <form action="" enctype="multipart/form-data" method="post">
                        <div class="form-group">
                            <label for=""> Full name</label>
                            <input class="form-control" name="full_name" type="text" value="<?= $user['full_name']?>"placeholder="Full name" required>
                        </div>
                        <div class="form-group">
                            <label for=""> User name</label>
                            <input class="form-control" name="user_name" type="text" value="<?= $user['user_name']?>" placeholder="user name" required>
                        </div>
                        <div class="form-group">
                            <label for=""> Email</label>
                            <input class="form-control" name="email" type="email" placeholder="Email" value="<?= $user['email']?>" required>
                        </div>
                        <div class="form-group">
                            <label for=""> Address</label>
                            <input class="form-control" name="address" type="text" placeholder="address" value="<?= $user['address']?>"required>
                        </div>
                        <div class="form-group">
                            <label for=""> Phone</label>
                            <input class="form-control" name="phone" type="text" placeholder="Phone" value="<?= $user['phone']?>"required>
                        </div>
                        <?php if($user['type'] == 1 ): ?>
                          <div class="form-group p-3">
                            <label for=""> Specialization</label>
                            <select class="form-select form-control" name="specialization" aria-label="Default select example">
                                <?php foreach($specializations as $specialization): ?> 
                              <option value="<?=$specialization['id']?>" <?=(isset($user['sspecialization_id']) && $user['sspecialization_id'] == $specialization['id'])? 'selected' : '';?>><?=$specialization['name']?></option>
                                  <?php endforeach; ?>
                            </select>                
                          </div>
                          <div class="form-group">
                            <label for=""> Description</label>
                            <textarea name="description" class="form-control" placeholder="Description" cols="30" rows="10"><?= $user['description']?></textarea>
                        </div>
                          <?php endif; ?>
                        <div class="form-group">
                        
                            <label for=""> Profile picture</label>
                            <br>
                            <img width="40" height="40"src="<?= (isset($user['profile_photo']) && !empty($user['profile_photo']))?  '/dashboard/images/profile_images/'.$user['profile_photo'] :'img/person_1.jpg';?>" alt="">
                            <br><br>
                            <input class="form-control" name="profile_photo" type="file" placeholder="profile photo">
                        </div>
  
						<input type="hidden" name="user_id" value="<?= $user['id']?>" >                 

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                  <button  name="update_user" type="submit" class="btn btn-primary search_btn">Save</button>

                </div>
                </form>

              </div>
            </div>
        </div>
</html>