
<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}    



    //intializing variabes
    $full_name = '';
    $user_name = '';
    $email = '';
    $phone = '';
    $address = '';

    //errors array
    $errors = array();
    //connect to db 
    include('connection/connection.php');
    if(isset($_POST['register_user'])){
        $full_name =(!empty($_POST['full_name']))? mysqli_real_escape_string($db , $_POST['full_name']): '';
        $user_name = (!empty($_POST['user_name']))? mysqli_real_escape_string($db , $_POST['user_name']) :'';
        $email = (!empty($_POST['email']))? mysqli_real_escape_string($db , $_POST['email']) :'';
        $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
        $type = (!empty($_POST['type']))? mysqli_real_escape_string($db , $_POST['type']) :1;
        $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
        $profile_photo = (!empty($_FILES['profile_photo']))? $_FILES['profile_photo'] :'';
        $password = (!empty($_POST['password']))? mysqli_real_escape_string($db , $_POST['password']): '';
        $password_confirmation = (!empty($_POST['password_confirmation']))? mysqli_real_escape_string($db , $_POST['password_confirmation']) : '';

        //form validation
        if(empty($full_name)){ array_push($errors , "- Full name is required");}
        if(empty($user_name)){ array_push($errors , "- User name is required");}
        if(empty($email)){ array_push($errors , "- Email is required");}
        if(empty($phone)){ array_push($errors , "- User phone is required");}
        if(empty($address)){ array_push($errors , "- User address is required");}
        if(empty($password)){ array_push($errors , "- Password is required");}
        if($password != $password_confirmation){ array_push($errors , "- Password should match password confirmation");}

        //check db for checking existing username

        $user_check_query = "SELECT * FROM users WHERE user_name = '$user_name' or email = '$email' LIMIT 1";
        $result = mysqli_query($db , $user_check_query);
        $user = mysqli_fetch_assoc($result);

        if($user){
            if($user['user_name'] === $user_name){ array_push($errors , 'User name already exists'); }
            if($user['email'] === $email){ array_push($errors , 'Email already exists'); }
        }
        $profile_photo = '';
        if(!empty($_FILES["profile_photo"]['name'])){
 
            $uploaddir = '/dashboard/images/profile_images/';
            $uploadfile = $uploaddir . basename($_FILES['profile_photo']['name']);
    
            echo "<p>";
            $file_upload = move_uploaded_file($_FILES['profile_photo']['tmp_name'], SITE_ROOT.$uploadfile);
            if ($file_upload) {
                $profile_photo = basename($_FILES['profile_photo']['name']);
            } else {
              array_push($errors , 'Course file upload failed'); 
            }
        }
        // register the user if no errors exist
        if(count($errors) == 0){

            $password = md5($password);
            $query = "INSERT INTO users (full_name ,user_name , email , password , type , phone , address , profile_photo) VALUES ('$full_name','$user_name' , '$email' , '$password' , $type , '$phone' , '$address' ,'$profile_photo')";
            $result_of_the_query = mysqli_query($db , $query);
            if($result_of_the_query == false){
                echo mysqli_error($db);
                die();
            }
            $_SESSION['full_name'] = $full_name;
            $_SESSION['success'] = 'You are logged in';
            $_SESSION['user_name'] = $user_name;
            $_SESSION['email'] = $email;
            $_SESSION['phone'] = $phone;
            $_SESSION['profile_photo'] = $profile_photo;
            $_SESSION['id'] = $db->insert_id;  
            $_SESSION['success'] = $type;
 
            header('location: index.php');
        }
    }

    //Loging in

    if(isset($_POST['login_user'])){
        $user_name =( mysqli_real_escape_string($db , $_POST['user_name']))? mysqli_real_escape_string($db , $_POST['user_name']) : '';
        $password = (mysqli_real_escape_string($db , $_POST['password']))? mysqli_real_escape_string($db , $_POST['password']): '';

        if(empty($user_name)) {
            array_push($errors , 'user name is required'); 
        }
        if(empty($password)) {
            array_push($errors , 'password is required'); 
        }
        if(empty($errors)){
            $password = md5($password);

            $query = "SELECT * FROM users WHERE user_name='$user_name' AND password='$password';";
            $result = mysqli_query($db ,$query);
            $user = mysqli_fetch_assoc($result);
            if(mysqli_num_rows($result)){
                
                $_SESSION['full_name'] = $user['full_name'];
                $_SESSION['user_name'] = $user['user_name'];
                $_SESSION['email'] = $user['email'];
                $_SESSION['phone'] = $user['phone'];
                $_SESSION['address'] = $user['address'];               
                 $_SESSION['success'] = "now you are logged in";
                 $_SESSION['success'] = $user['type'];
                $_SESSION['id'] = $user['id'];
                $_SESSION['type'] = $user['type'];

                if($user['type'] == 0){
                    header('location: /dashboard/index.php'); 
                }else{
                    header('location: index.php');
                }
            }else{
                array_push($errors , "wrong username or password");
            }
        }
    }
    // update user
    if(isset($_POST['update_user'])){
        $full_name =(!empty($_POST['full_name']))? mysqli_real_escape_string($db , $_POST['full_name']): '';
        $user_name = (!empty($_POST['user_name']))? mysqli_real_escape_string($db , $_POST['user_name']) :'';
        $email = (!empty($_POST['email']))? mysqli_real_escape_string($db , $_POST['email']) :'';
        $phone = (!empty($_POST['phone']))? mysqli_real_escape_string($db , $_POST['phone']) :'';
        $address = (!empty($_POST['address']))? mysqli_real_escape_string($db , $_POST['address']) :'';
        $specialization = (!empty($_POST['specialization']))? mysqli_real_escape_string($db , $_POST['specialization']) :2;
        $description = (!empty($_POST['description']))? mysqli_real_escape_string($db , $_POST['description']) :'';

        $profile_photo = '';
        $user_id =(!empty($_POST['user_id']))? mysqli_real_escape_string($db , $_POST['user_id']): '';

        if(empty($full_name)){ array_push($errors , "- Full name is required");}
        if(empty($user_name)){ array_push($errors , "- User name is required");}
        if(empty($email)){ array_push($errors , "- Email is required");}
        if(empty($phone)){ array_push($errors , "- User phone is required");}
        if(empty($address)){ array_push($errors , "- User address is required");}

        //check db for checking existing username
        //check db for checking existing username

        $user_check_query = "SELECT * FROM users WHERE (user_name = '$user_name' or email = '$email') and id != '$user_id' LIMIT 1";
        $result = mysqli_query($db , $user_check_query);
        $user = mysqli_fetch_assoc($result);

        if($user){
            if($user['user_name'] === $user_name){ array_push($errors , 'User name already exists'); }
            if($user['email'] === $email){ array_push($errors , 'Email already exists'); }
        }
        if(!empty($_FILES["profile_photo"]['name'])){
     
            $uploaddir = '/dashboard/images/profile_images/';
            $uploadfile = $uploaddir . basename($_FILES['profile_photo']['name']);
    
            echo "<p>";
            $file_upload = move_uploaded_file($_FILES['profile_photo']['tmp_name'], SITE_ROOT.$uploadfile);
            if ($file_upload) {
                $profile_photo = basename($_FILES['profile_photo']['name']);
            } else {
              array_push($errors , 'image upload failed'); 
            }
        }

        // register the user if no errors exist
        if(count($errors) == 0){
            if(!empty($profile_photo)):
                    $query = "UPDATE users SET full_name = '$full_name' , user_name = '$user_name' ,  address = '$address' , phone = '$phone' , email='$email' , specialization = '$specialization' , description = '$description', profile_photo = '$profile_photo' Where id = $user_id";
            else:
                $query = "UPDATE users SET full_name = '$full_name' , user_name = '$user_name' ,  address = '$address' , phone = '$phone' , email='$email' , specialization =' $specialization' , description = '$description'  Where id = $user_id";      
            endif;
            $result_of_the_query = mysqli_query($db , $query);
            if($result_of_the_query == false){
                echo mysqli_error($db);
                die();
            }
            $course = $result ->fetch_array(MYSQLI_ASSOC);
          
            $success = "users updated successfully";
            header('location: /my-profile.php');
        }
    }