<?php 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(isset($_GET['logout'])){
    session_destroy();
    unset($_SESSION['full_name']);
    header('location: login.php');
}
?>
<nav class="navbar navbar-default probootstrap-navbar">
        <div class="container">
          <div class="navbar-header">
            <div class="btn-more js-btn-more visible-xs">
              <a href="#"><i class="icon-dots-three-vertical "></i></a>
            </div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="index.php" style="font-size: 50px;
            line-height: normal;">Selflearn</a>
          </div>

          <div id="navbar-collapse" class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-right">
                <li><p style="<?= ( isset($_SESSION['profile_photo']))?'padding-top:20px;':'padding-top:30px;'?>">
                <?php if(isset($_SESSION['profile_photo'])): ?>
                <img width="50" height="50"src="<?= (isset($_SESSION['profile_photo']))?  '/dashboard/images/profile_images/'.$_SESSION['profile_photo'] :'';?>" alt="">
                <?php endif;?>
                <?php if(isset($_SESSION['user_name'])){echo '<a href="/my-profile.php">Welcome '.$_SESSION['user_name'] . '</a>';}?></p></li>
              <li class="<?= ($_SERVER['REQUEST_URI'] == "/index.php")? 'active' : '';?>"><a href="index.php">Home</a></li>
              <li class="<?= ($_SERVER['REQUEST_URI'] == "/courses.php")? 'active' : '';?>"><a href="courses.php">Courses</a></li>
              <li class="<?= ($_SERVER['REQUEST_URI'] == "/teachers.php")? 'active' : '';?>" ><a href="teachers.php">Teachers</a></li>
              <li class="<?= ($_SERVER['REQUEST_URI'] == "/contact.php")? 'active' : '';?>"><a href="contact.php">Contact</a></li>
              <?php if(!isset($_SESSION['success'])) : ?>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/register.php")? 'active' : '';?>"><a href="register.php">Register</a></li>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/login.php")? 'active' : '';?>"><a href="login.php">Login</a></li>                      
              <?php else:?>                           
              <li><a href="index.php?logout=true">LOGOUT </a></li>                      
              <?php endif;?>                           
            </ul>
          </div>
        </div>
      </nav>