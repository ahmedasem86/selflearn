<!DOCTYPE html>
<html lang="en">
<?php 
include('head.php');
include('dashboard/get_courses.php');
include('dashboard/get_course_cat.php');
include('dashboard/get_course_grade.php');

?>
  <body>

  
    <div class="probootstrap-page-wrapper">
      <!-- Fixed navbar -->
      <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1>Our Courses</h1>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section" style="padding:1em;">
        <div class="container">
          <div class="row">
            <div class="col-md-12"> 
                <form class="form-inline">
                  <div class="col-md-3">
                    <select class=" form-control" name="category" id="">
                      <option value="all">Select Course type</option>
                      <?php foreach($courses_categories as $category): ?>
                      <option value="<?= $category['id'];?>" <?= (isset($_GET['category']) && $_GET['category'] == $category['id'])? 'selected' : ''; ?>><?= $category['name'];?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <select class=" form-control" name="grade" id="">
                      <option value="all">Select Grade</option>
                      <?php foreach($courses_grades as $grade): ?>
                      <option value="<?= $grade['id'];?>" <?= (isset($_GET['grade']) && $_GET['grade'] == $grade['id'])? 'selected' : ''; ?>><?= $grade['name'];?></option>
                        <?php endforeach; ?>
                    </select>
                  </div>
                  <input class="form-control mr-sm-2" name="keyword" value="<?= (isset($_GET['keyword']))? $_GET['keyword'] : ''?>" type="search" placeholder="Keyword" aria-label="Search">
                  <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                </form>
                <?php if(isset($_SESSION['id'])): ?>
                  <div class="pull-right ">
                  <a href="add_course.php" class="btn btn-primary my-2 my-sm-0"> Add new course</a>
                  </div>
                <?php endif;?>
              </div>  
          </div>
        </div>
      </section>

      <section class="probootstrap-section" style="padding-top:1em;">
        <div class="container">
          <div class="row">
            <?php foreach($courses as $course):  ?>
              <div class="col-md-6">
              <div class=" probootstrap-service-2 probootstrap-animate">
                <div class="image">
                  <div class="image-bg">
                    <img style="max-height:245px;"src="dashboard/images/course_images/<?= (!empty($course['image']))? $course['image'] : 'download.jpeg'?>" alt="Course images">
                  </div>
                </div>
                <div class="text">
                  <span class="probootstrap-meta"><i class="icon-calendar2"></i> <?= $course['uploaded_date']?></span>
                  <h3><?= $course['name']?></h3>
                  <p style="height:90px;"><?= substr($course['description'], 0, strrpos(substr($course['description'], 0, 100), ' '));?>...</p>
                  <p><a href="course-single.php?course_id=<?= $course['id']?>" class="btn btn-primary">View Course</a></p>
                </div>
              </div>
              </div>
              <?php endforeach;?>
          </div>
        </div>
      </section>

      <?php include('footer.php');?>

    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>