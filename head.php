<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Self learning</title>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <?php
    // error_reporting(E_ERROR | E_PARSE);
if(!defined ('SITE_ROOT')){
  define ('SITE_ROOT', realpath(dirname(__FILE__)));
} include('functions.php');
include('dashboard/functions.php');
?>
  </head>