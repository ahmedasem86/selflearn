<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>

  <body>

    
    <div class="probootstrap-page-wrapper">
      <!-- Fixed navbar -->
      <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1 class="mb0">Register</h1>
            </div>
          </div>
        </div>
      </section>
      <section class="probootstrap-section probootstrap-section-sm">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="row probootstrap-gutter0">
                <div class="col-md-6 col-md-push-3  probootstrap-animate" id="probootstrap-content">
                  <h2>Register</h2>
                  <?php include('errors.php');?>

                  <p>Register your own account now</p>
                  <form action="#" method="post" class="probootstrap-form" enctype="multipart/form-data">
                    <div class="form-group">
                      <label for="name">Full Name</label>
                      <input type="text" class="form-control" id="full_name" name="full_name" required>
                    </div>
                    <div class="form-group">
                      <label for="name">User Name</label>
                      <input type="text" class="form-control" id="user_name" name="user_name" required>
                    </div>
                    <div class="form-group">
                      <label for="name">Address</label>
                      <input type="text" class="form-control" id="address" name="address" required>
                    </div>
                    <div class="form-group">
                      <label for="name">Profile pic</label>
                      <input type="file" class="form-control"  name="profile_photo" >
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group p-3">
                    <label for=""> User Type</label>
                    <select class="form-select form-control" name="type" aria-label="Default select example">
                        <option value="1">Teacher</option>
                        <option value="2">Student</option>
                    </select>                
                  </div>
                    <div class="form-group">
                      <label for="subject">Phone number</label>
                      <input type="number" class="form-control" id="phone" name="phone" required>
                    </div>
                    <div class="form-group">
                      <label for="name">Password</label>
                      <input type="password" class="form-control" id="password" name="password" required>
                    </div>                   
                     <div class="form-group">
                      <label for="name">Confirm Password</label>
                      <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                    </div>

                    <div class="form-group">
                      <input type="submit" class="btn btn-primary btn-lg" id="submit" name="register_user" value="Register">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('footer.php');?>


    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

    
  </body>
</html>