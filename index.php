<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>
<?php include('dashboard/get_teachers.php'); ?>
<?php include('dashboard/get_courses.php'); ?>

  <body>

    <div class="probootstrap-page-wrapper">
      <!-- Fixed navbar -->
    
      <?php include('header.php'); ?>

      <section class="flexslider">
        <ul class="slides">
          <li style="background-image: url(img/slider_1.jpg)" class="overlay">
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="probootstrap-slider-text text-center">
                    <h1 class="probootstrap-heading probootstrap-animate">Your Bright Future is Our Mission</h1>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li style="background-image: url(img/slider_2.jpg)" class="overlay">
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="probootstrap-slider-text text-center">
                    <h1 class="probootstrap-heading probootstrap-animate">Education is Life</h1>
                  </div>
                </div>
              </div>
            </div>
            
          </li>
          <li style="background-image: url(img/slider_3.jpg)" class="overlay">
            <div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="probootstrap-slider-text text-center">
                    <h1 class="probootstrap-heading probootstrap-animate">Helping Each of Our Students Fulfill the Potential</h1>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </section>
      
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h2>Welcome to School of Excellence</h2>
            </div>
          </div>
        </div>
      </section>

      <section class="probootstrap-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="probootstrap-flex-block">
                <div class="probootstrap-text probootstrap-animate" style="width:100%;">
                  <h3>About Self Learning</h3>
                  <p>Online Self-Learning is a website to enable teachers to give their experience and knowledge to their students and to share links, documents, YouTube videos, and exams in many subjects.</p>
                  <p><a href="contact.php" class="btn btn-primary">Contact Self Learning</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="probootstrap-section probootstrap-bg-white probootstrap-border-top">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
              <h2>Our Featured Courses</h2>
              <!-- <p class="lead"></p> -->
            </div>
          </div>
          <!-- END row -->
          <div class="row">
          <?php foreach($courses as $i=> $course):  ?>
              <div class="col-md-6">
              <div class=" probootstrap-service-2 probootstrap-animate">
                <div class="image">
                  <div class="image-bg">
                    <img style="max-height:245px;"src="dashboard/images/course_images/<?= (!empty($course['image']))? $course['image'] : 'download.jpeg'?>" alt="Course images">
                  </div>
                </div>
                <div class="text">
                  <span class="probootstrap-meta"><i class="icon-calendar2"></i> <?= $course['uploaded_date']?></span>
                  <h3><?= $course['name']?></h3>
                  <p style="height:90px;"><?= substr($course['description'], 0, strrpos(substr($course['description'], 0, 100), ' '));?>...</p>
                  <p><a href="course-single.php?course_id=<?= $course['id']?>" class="btn btn-primary">View Course</a></p>
                </div>
              </div>
              </div>

              <?php if($i==3){break;} endforeach;?>
            <!-- <div class="col-md-6">
              <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                  <div class="image-bg">
                    <img src="img/img_sm_2.jpg" alt="My images">
                  </div>
                </div>
                <div class="text">
                  <span class="probootstrap-meta"><i class="icon-calendar2"></i> July 10, 2017</span>
                  <h3>Math Major</h3>
                  <p>Laboriosam pariatur modi praesentium deleniti molestiae officiis atque numquam quos quis nisi voluptatum architecto rerum error.</p>
                  <p><a href="course-single.php" class="btn btn-primary">View Course</a></p>
                </div>
              </div>

              <div class="probootstrap-service-2 probootstrap-animate">
                <div class="image">
                  <div class="image-bg">
                    <img src="img/img_sm_4.jpg" alt="My images">
                  </div>
                </div>
                <div class="text">
                  <span class="probootstrap-meta"><i class="icon-calendar2"></i> July 10, 2017</span>
                  <h3>English Major</h3>
                  <p>Laboriosam pariatur modi praesentium deleniti molestiae officiis atque numquam quos quis nisi voluptatum architecto rerum error.</p>
                  <p><a href="course-single.php" class="btn btn-primary">View Course</a></p>
                </div>
              </div>

            </div> -->
          </div>
        </div>
      </section>

      
      
      <section class="probootstrap-section" style="padding-top: 0px;">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
              <h2>Meet Our Qualified Teachers</h2>
              <!-- <p class="lead">Sed a repudiandae impedit voluptate nam Deleniti dignissimos perspiciatis nostrum porro nesciunt</p> -->
            </div>
          </div>
          <!-- END row -->

          <div class="row">
            <?php foreach($teachers as $teacher): ?>
              <a href="my-profile.php?user_id=<?= $teacher['id']?>">
            <div class="col-md-3 col-sm-6">
              <div class="probootstrap-teacher text-center probootstrap-animate">
                <figure class="media">
                  <img style="height: 90px;"src="<?= (isset($teacher['profile_photo']) && !empty($teacher['profile_photo']))?  '/dashboard/images/profile_images/'.$teacher['profile_photo'] :'img/person_1.jpg';?>" alt="My images" class="img-responsive">
                </figure>
                <div class="text">
                  <h3><?= $teacher['full_name'];?></h3>
                  <p><?= (isset($teacher['name'])?$teacher['name']:'')?> Teacher</p>

                </div>
              </div>
            </div>
            </a>
            <?php endforeach;?>
          </div>

        </div>
      </section>
      <?php include('footer.php'); ?>

    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>