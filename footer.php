<footer class="probootstrap-footer probootstrap-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="probootstrap-footer-widget">
                <h3>About Self-learning</h3>
                <p>Online Self-Learning is a website to enable teachers to give their experience and knowledge to their students and to share links, documents, YouTube videos, and exams in many subjects.</p>
              </div>
            </div>
            <div class="col-md-3 col-md-push-1">
              <div class="probootstrap-footer-widget">
                <h3>Links</h3>
                <ul>
                  <li><a href="index.php">Home</a></li>
                  <li><a href="courses.php">Courses</a></li>
                  <li><a href="teachers.php">Teachers</a></li>
                  <li><a href="register.php">Register</a></li>
                  <li><a href="login.php">Login</a></li>
                  <li><a href="contact.php">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4">
              <div class="probootstrap-footer-widget">
                <h3>Contact Info</h3>
                <ul class="probootstrap-contact-info">
                  <li><i class="icon-location2"></i> <span>198 kuwait Street, kuwait</span></li>
                  <li><i class="icon-mail"></i><span> info@onlinelearningkw.com</span></li>
                  <li><i class="icon-phone2"></i><span>+965-23456789</span></li>
                </ul>
              </div>
            </div>
           
          </div>
          <!-- END row -->
          
        </div>
      </footer>