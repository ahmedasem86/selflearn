<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); ?>

  <body>

    
    <div class="probootstrap-page-wrapper">
      <!-- Fixed navbar -->
      <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1 class="mb0">Login</h1>
            </div>
          </div>
        </div>
      </section>
      <section class="probootstrap-section probootstrap-section-sm">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="row probootstrap-gutter0">
                <div class="col-md-6 col-md-push-3  probootstrap-animate" id="probootstrap-content">
 
                <h2>Login</h2>
                <?php include('errors.php');?>

                  <form action="#" method="post" class="probootstrap-form">
                    <div class="form-group">
                      <label for="USER NAME">User name</label>
                      <input type="text" class="form-control" id="user_name" name="user_name">
                    </div>
                    <div class="form-group">
                      <label for="name">Password</label>
                      <input type="password" class="form-control" id="password" name="password">
                    </div>                   
                    <div class="form-group">
                      <input type="submit" class="btn btn-primary btn-lg" id="submit" name="login_user" value="Login">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('footer.php');?>

    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

    
  </body>
</html>