<!DOCTYPE html>
<html lang="en">
<?php include('head.php'); 
include('dashboard/get_course_by_id.php');
$course_data = $course;
include('dashboard/get_courses.php');
?>
  <body>
    <div class="probootstrap-page-wrapper">
    <?php include('header.php'); ?>
      <section class="probootstrap-section probootstrap-section-colored">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-left section-heading probootstrap-animate">
              <h1><?= $course_data['name'];?></h1>
            </div>
          </div>
        </div>
      </section>

      

      <section class="probootstrap-section probootstrap-section-sm">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="row probootstrap-gutter0">
                <div class="col-md-4" id="probootstrap-sidebar">
                  <div class="probootstrap-sidebar-inner probootstrap-overlap probootstrap-animate">
                    <h3>More Courses</h3>
                    <ul class="probootstrap-side-menu">
                    <?php foreach($courses as $course): ?>
                      <li class="<?= ($course_data['id'] == $course['id'])? 'active' :''; ?>">
                        <a href="course-single.php?course_id=<?= $course['id']?>">
                          <?= $course['name'] ?>
                        </a>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                </div>
                <div class="col-md-7 col-md-push-1  probootstrap-animate" id="probootstrap-content">
                  <h2>Description</h2>
                  <p><?= $course_data['description'];?></p>
                  <p>
                      <?php if($course_data['type'] == 0 ): ?>
                        <a href="/dashboard/courses/<?=$course_data['source_file']?>" class="btn btn-primary">View Pdf</a>
                        <?php elseif($course_data['type'] == 1 ): ?>
                        <a href="/dashboard/courses/<?=$course_data['source_file']?>" class="btn btn-primary">View Word</a> 
                        <?php elseif($course_data['type'] == 3 ): ?>
                        <a href="/dashboard/courses/<?=$course_data['source_file']?>" class="btn btn-primary">View Image</a> 
                        <?php elseif($course_data['type'] == 4 ): ?>
                        <a href="/dashboard/courses/<?=$course_data['source_file']?>" class="btn btn-primary">View Power Point</a> 

                        <?php endif;?>
                    </p>
                    <?php if($course_data['type'] == 2 ): ?>
                     <iframe src="<?=$course_data['file_url']?>" width="560" height="315" frameborder="0" auto-play allowfullscreen></iframe>
                    <video src="<?=$course_data['file_url']?>"></video>
                     <?php endif;?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('footer.php');?>

    </div>
    <!-- END wrapper -->
    

    <script src="js/scripts.min.js"></script>
    <script src="js/main.min.js"></script>
    <script src="js/custom.js"></script>

  </body>
</html>